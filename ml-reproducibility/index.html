<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Martin Thoma" />
        <meta name="copyright" content="Martin Thoma" />
        <link title = "Martin Thoma"
              type  = "application/opensearchdescription+xml"
              rel   = "search"
              href  = "../opensearch.xml">

        <meta property="og:type" content="article" />
        <meta name="twitter:card" content="summary">

<meta name="keywords" content="Machine Learning, Research, Machine Learning, " />

<meta property="og:title" content="Reproducibility in Machine Learning "/>
<meta property="og:url" content="../ml-reproducibility/" />
<meta property="og:description" content="Getting reproducible results is important because of trust: Why should somebody else trust you, if you can get the same results repeatedly? Why do you trust your results in the first place? People make errors. Making sure you can repeat what you did before eliminates possibilities for human error. Here …" />
<meta property="og:site_name" content="Martin Thoma" />
<meta property="og:article:author" content="Martin Thoma" />
<meta property="og:article:published_time" content="2017-12-13T20:00:00+01:00" />
<meta name="twitter:title" content="Reproducibility in Machine Learning ">
<meta name="twitter:description" content="Getting reproducible results is important because of trust: Why should somebody else trust you, if you can get the same results repeatedly? Why do you trust your results in the first place? People make errors. Making sure you can repeat what you did before eliminates possibilities for human error. Here …">
<meta property="og:image" content="logos/ml.png" />
<meta name="twitter:image" content="logos/ml.png" >

        <title>Reproducibility in Machine Learning  · Martin Thoma
</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../theme/css/pygments.css" media="screen">
        <link rel="stylesheet" type="text/css" href="../theme/tipuesearch/tipuesearch.css" media="screen">
        <link rel="stylesheet" type="text/css" href="../theme/css/elegant.css" media="screen">
        <link rel="stylesheet" type="text/css" href="../static/custom.css" media="screen">

        <!-- MathJax -->
<script type="text/x-mathjax-config">
<!--
MathJax.Hub.Config({
  jax: ["input/TeX", "output/HTML-CSS"],
  tex2jax: {
    inlineMath: [['$','$'], ['\\(','\\)']],
    displayMath: [ ['$$', '$$'], ['\\[','\\]']],
    skipTags: ['script', 'noscript', 'style', 'textarea', 'pre', 'code'],
    processEscapes: true
  }
});

MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax(), i;
    for(i=0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
// -->
</script>
<script type="text/javascript" async
  src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML">
</script>

        <link href="https://martin-thoma.com/feeds/all.atom.xml" type="application/atom+xml" rel="alternate" title="Martin Thoma - Full Atom Feed" />
        <link href="https://martin-thoma.com/feeds/index.xml" type="application/rss+xml" rel="alternate" title="Martin Thoma - Full RSS Feed" />
    </head>
    <body>
        <div id="content-sans-footer">
        <div class="navbar navbar-static-top navbar-default">
            <div class="container">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse">
                        <ul class="nav pull-left top-menu navbar-nav">
                            <li><a href=".." style="font-family: 'Monaco', 'Inconsolata', 'Andale Mono', 'Lucida Console', 'Bitstream Vera Sans Mono', 'Courier New', Courier, Monospace;
                        font-size: 20px;" class="navbar-brand">Martin Thoma</a>
                            </li>
                        </ul>
                        <ul class="nav pull-right top-menu navbar-nav">
                            <li ><a href="..">Home</a></li>
                            <li ><a href="../categories.html">Categories</a></li>
                            <li ><a href="../tags.html">Tags</a></li>
                            <li ><a href="../archives.html">Archives</a></li>
                            <li><a href="../support-me/">Support me</a></li>
                            <li><form class="navbar-form" action="../search.html" onsubmit="return validateForm(this.elements['q'].value);"> <input type="search" class="search-query form-control" placeholder="Search" name="q" id="tipue_search_input"></form></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-1 col-md-1"></div>
                <div class="col-sm-10 col-md-10">
<article>
<div class="row">
    <header class="page-header col-sm-10 col-md-10 col-md-offset-2">
    <h1><a href="../ml-reproducibility/"> Reproducibility in Machine Learning  </a></h1>
    </header>
</div>

<div class="row">
    <div class="col-sm-2 col-md-2 table-of-content">
        <nav>
        <h4>Contents</h4>
        <div id="toc"><ul><li><a class="toc-href" href="#writing-a-paper" title="Writing a paper">Writing a paper</a></li></ul></div>
        </nav>
    </div>
    <div class="col-sm-8 col-md-8 article-content" id="contentAfterTitle">

            
            <p>Getting reproducible results is important because of trust: Why should somebody
else trust you, if you can get the same results repeatedly? Why do you trust
your results in the first place? People make errors. Making sure you can repeat
what you did before eliminates possibilities for human error.</p>
<p>Here are possible reasons why the results of machine learning projects are not
always the same. They are roughly ordered from most likely/easiest to fix to
most unlikely/hardest to fix. I also try to give a solution after the problem:</p>
<ol>
<li><strong>Human error</strong> - you missread a number / made a typo when you copied a result from one shell to the paper: Logging. Create an <code>2017-12-31-23-54-experiment-result.log</code> for every single experiment you run. Not manually,
 but the experiment creates it. Yes, the time stamp in the name for easier finding it again. All following should be logged to that file for each single experiment.</li>
<li><strong>Code</strong> changed: Version control (e.g. git)</li>
<li><strong>Configuration file</strong> changed: Version control</li>
<li><strong>Pseudorandom number</strong> changed: set seed for random / tensorflow / numpy (yes, you might have to set more than one seed)</li>
<li><strong>Data loading</strong> differently / in a different order: Version control + seed (is the preprocessing really the same?)</li>
<li><strong>Environment variables</strong> changed: Docker</li>
<li><strong>Software (version)</strong> changed: Docker</li>
<li><strong>Driver (version)</strong> changed: Logging</li>
<li><strong>Hardware</strong> changed: Logging</li>
<li><strong>Concurrency</strong>: The fact that <a href="https://en.wikipedia.org/wiki/Associative_property#Nonassociativity_of_floating_point_calculation">floating point multiplication is not associative</a> and different cores on a GPU might finish computations at different times</li>
<li><strong>Hardware has errors</strong></li>
</ol>
<p>In any case, running the "same" thing multiple times might help to get a gut
feeling for how different things are.</p>
<h2 id="writing-a-paper">Writing a paper</h2>
<p>If you write a paper, I think the following would be the best practice for reproducibility:</p>
<ol>
<li>Add a link to a <strong>repository</strong> (e.g. git) where all code is</li>
<li>The code has to be <strong>containerized</strong> (e.g. Docker)</li>
<li>If there is Python code and a <code>requirements.txt</code> you have to give the <strong>exact software version</strong>, not something like <code>tensorflow&gt;=1.0.0</code> but <code>tensorflow==1.2.3</code></li>
<li>Add the <strong>git hash</strong> of the version you used for the experiments. It might be different hashes if you changed something in between.</li>
<li>Always log information about <strong>drivers</strong> (e.g. <a href="https://stackoverflow.com/a/47781255/562769">like this for nVidia</a>) and <strong>hardware</strong>. Add this to the appendix of your paper. So in case of later changes one can at least check if there was a change which might cause numbers being different.</li>
</ol>
<p>For logging the versions, you might want to use something like this:</p>
<table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37</pre></div></td><td class="code"><div class="highlight"><pre><span></span><span class="ch">#!/usr/bin/env python</span>

<span class="c1"># core modules</span>
<span class="kn">import</span> <span class="nn">subprocess</span>


<span class="k">def</span> <span class="nf">get_logstring</span><span class="p">():</span>
    <span class="sd">"""</span>
<span class="sd">    Get important environment information that might influence experiments.</span>

<span class="sd">    Returns</span>
<span class="sd">    -------</span>
<span class="sd">    logstring : str</span>
<span class="sd">    """</span>
    <span class="n">logstring</span> <span class="o">=</span> <span class="p">[]</span>
    <span class="k">with</span> <span class="nb">open</span><span class="p">(</span><span class="s1">'/proc/cpuinfo'</span><span class="p">)</span> <span class="k">as</span> <span class="n">f</span><span class="p">:</span>
        <span class="n">cpuinfo</span> <span class="o">=</span> <span class="n">f</span><span class="o">.</span><span class="n">readlines</span><span class="p">()</span>
    <span class="k">for</span> <span class="n">line</span> <span class="ow">in</span> <span class="n">cpuinfo</span><span class="p">:</span>
        <span class="k">if</span> <span class="s2">"model name"</span> <span class="ow">in</span> <span class="n">line</span><span class="p">:</span>
            <span class="n">logstring</span><span class="o">.</span><span class="n">append</span><span class="p">(</span><span class="s2">"CPU: {}"</span><span class="o">.</span><span class="n">format</span><span class="p">(</span><span class="n">line</span><span class="o">.</span><span class="n">strip</span><span class="p">()))</span>
            <span class="k">break</span>

    <span class="k">with</span> <span class="nb">open</span><span class="p">(</span><span class="s1">'/proc/driver/nvidia/version'</span><span class="p">)</span> <span class="k">as</span> <span class="n">f</span><span class="p">:</span>
        <span class="n">version</span> <span class="o">=</span> <span class="n">f</span><span class="o">.</span><span class="n">read</span><span class="p">()</span><span class="o">.</span><span class="n">strip</span><span class="p">()</span>
    <span class="n">logstring</span><span class="o">.</span><span class="n">append</span><span class="p">(</span><span class="s2">"GPU driver: {}"</span><span class="o">.</span><span class="n">format</span><span class="p">(</span><span class="n">version</span><span class="p">))</span>
    <span class="n">logstring</span><span class="o">.</span><span class="n">append</span><span class="p">(</span><span class="s2">"VGA: {}"</span><span class="o">.</span><span class="n">format</span><span class="p">(</span><span class="n">find_vga</span><span class="p">()))</span>
    <span class="k">return</span> <span class="s2">"</span><span class="se">\n</span><span class="s2">"</span><span class="o">.</span><span class="n">join</span><span class="p">(</span><span class="n">logstring</span><span class="p">)</span>


<span class="k">def</span> <span class="nf">find_vga</span><span class="p">():</span>
    <span class="n">vga</span> <span class="o">=</span> <span class="n">subprocess</span><span class="o">.</span><span class="n">check_output</span><span class="p">(</span><span class="s2">"lspci | grep -i 'vga\|3d\|2d'"</span><span class="p">,</span>
                                  <span class="n">shell</span><span class="o">=</span><span class="bp">True</span><span class="p">,</span>
                                  <span class="n">executable</span><span class="o">=</span><span class="s1">'/bin/bash'</span><span class="p">)</span>
    <span class="k">return</span> <span class="n">vga</span>


<span class="k">print</span><span class="p">(</span><span class="n">get_logstring</span><span class="p">())</span>
</pre></div>
</td></tr></table>
<p>which gives something like</p>
<div class="highlight"><pre><span></span><span class="n">CPU</span><span class="o">:</span> <span class="n">model</span> <span class="n">name</span>    <span class="o">:</span> <span class="n">Intel</span><span class="o">(</span><span class="n">R</span><span class="o">)</span> <span class="n">Core</span><span class="o">(</span><span class="n">TM</span><span class="o">)</span> <span class="n">i7</span><span class="o">-</span><span class="mi">6700</span><span class="n">HQ</span> <span class="n">CPU</span> <span class="err">@</span> <span class="mf">2.60</span><span class="n">GHz</span>
<span class="n">GPU</span> <span class="n">driver</span><span class="o">:</span> <span class="n">NVRM</span> <span class="n">version</span><span class="o">:</span> <span class="n">NVIDIA</span> <span class="n">UNIX</span> <span class="n">x86_64</span> <span class="n">Kernel</span> <span class="n">Module</span>  <span class="mf">384.90</span>  <span class="n">Tue</span> <span class="n">Sep</span> <span class="mi">19</span> <span class="mi">19</span><span class="o">:</span><span class="mi">17</span><span class="o">:</span><span class="mi">35</span> <span class="n">PDT</span> <span class="mi">2017</span>
<span class="n">GCC</span> <span class="n">version</span><span class="o">:</span>  <span class="n">gcc</span> <span class="n">version</span> <span class="mf">5.4</span><span class="o">.</span><span class="mi">0</span> <span class="mi">20160609</span> <span class="o">(</span><span class="n">Ubuntu</span> <span class="mf">5.4</span><span class="o">.</span><span class="mi">0</span><span class="o">-</span><span class="mi">6</span><span class="n">ubuntu1</span><span class="o">~</span><span class="mf">16.04</span><span class="o">.</span><span class="mi">5</span><span class="o">)</span>
<span class="n">VGA</span><span class="o">:</span> <span class="mi">00</span><span class="o">:</span><span class="mf">02.0</span> <span class="n">VGA</span> <span class="n">compatible</span> <span class="n">controller</span><span class="o">:</span> <span class="n">Intel</span> <span class="n">Corporation</span> <span class="n">Skylake</span> <span class="n">Integrated</span> <span class="n">Graphics</span> <span class="o">(</span><span class="n">rev</span> <span class="mi">06</span><span class="o">)</span>
<span class="mi">02</span><span class="o">:</span><span class="mf">00.0</span> <span class="mi">3</span><span class="n">D</span> <span class="n">controller</span><span class="o">:</span> <span class="n">NVIDIA</span> <span class="n">Corporation</span> <span class="n">GM108M</span> <span class="o">[</span><span class="n">GeForce</span> <span class="mi">940</span><span class="n">MX</span><span class="o">]</span> <span class="o">(</span><span class="n">rev</span> <span class="n">a2</span><span class="o">)</span>
</pre></div>
            
            <div id="disqus_thread"></div>
<script>
    /*
    var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() {  // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
        var d = document, s = d.createElement('script');

        s.src = '//martinthoma.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

            <hr/>
        </div>
        <section>
        <div class="col-sm-2 col-md-2" style="float:right;font-size:0.9em;">
            <h4>Published</h4>
            <time pubdate="pubdate" datetime="2017-12-13T20:00:00+01:00">Dez 13, 2017</time>
            <br/>
            by <a rel="author" class="vcard author post-author" itemprop="author" href="../author/martin-thoma/"><span class="fn" itemscope="" itemtype="https://schema.org/Person"><span itemprop="name">Martin Thoma</span></span></a>
            <h4>Category</h4>
            <a class="category-link" href="../categories.html#machine-learning-ref">Machine Learning</a>
            <h4>Tags</h4>
            <ul class="list-of-tags tags-in-article">
                <li><a href="../tags.html#machine-learning-ref">Machine Learning
                    <span>67</span>
</a></li>
                <li><a href="../tags.html#research-ref">Research
                    <span>3</span>
</a></li>
            </ul>
<h4>Contact</h4>
    <a href="https://twitter.com/themoosemind" title="My Twitter Profile" class="sidebar-social-links" target="_blank">
    <i class="fa fa-twitter sidebar-social-links"></i></a>
    <a href="mailto:info@martin-thoma.de" title="My Email Address" class="sidebar-social-links" target="_blank">
    <i class="fa fa-envelope sidebar-social-links"></i></a>
    <a href="https://github.com/MartinThoma" title="My Github Profile" class="sidebar-social-links" target="_blank">
    <i class="fa fa-github sidebar-social-links"></i></a>
    <a href="http://stackoverflow.com/users/562769/martin-thoma" title="My Stackoverflow Profile" class="sidebar-social-links" target="_blank">
    <i class="fa fa-stackoverflow sidebar-social-links"></i></a>
        </div>
        </section>
</div>
</article>
                </div>
                <div class="col-sm-1 col-md-1"></div>
            </div>
        </div>
        <div id="push"></div>
    </div>
<footer>
<div id="footer">
    <ul class="footer-content">
        <li class="elegant-subtitle"><span class="site-name">Martin Thoma</span> - A blog about Code, the Web and Cyberculture</li>
        <li><a href="http://www.martin-thoma.de/privacy.htm">Datenschutzerkl&auml;rung</a></li>
        <li><a href="http://www.martin-thoma.de/impressum.htm">Impressum</a></li>
        <li class="elegant-power">Powered by <a href="http://getpelican.com/" title="Pelican Home Page">Pelican</a>. Theme: <a href="http://oncrashreboot.com/pelican-elegant" title="Theme Elegant Home Page">Elegant</a> by <a href="http://oncrashreboot.com" title="Talha Mansoor Home Page">Talha Mansoor</a></li>
    </ul>
</div>
</footer>        <script src="//code.jquery.com/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            function validateForm(query)
            {
                return (query.length > 0);
            }
        </script>
        <script>
(function(){
    'use strict';

    /*
    Create intra-page links
    Requires that your headings already have an `id` attribute set (because that's what jekyll does)
    For every heading in your page, this adds a little anchor link `#` that you can click to get a permalink to the heading.
    Ignores `h1`, because you should only have one per page.
    The text content of the tag is used to generate the link, so it will fail "gracefully-ish" if you have duplicate heading text.

    Credit: https://gist.github.com/SimplGy/a229d25cdb19d7f21231
     */

    var headingNodes = [], results, link,
        tags = ['h2', 'h3', 'h4', 'h5', 'h6'];

    tags.forEach(function(tag){
        var contentTag = document.getElementById('contentAfterTitle');
      results = contentTag.getElementsByTagName(tag);
      Array.prototype.push.apply(headingNodes, results);
    });

    headingNodes.forEach(function(node){
      link = document.createElement('a');
      link.className = 'deepLink';
      link.textContent = ' ¶';
      link.href = '#' + node.getAttribute('id');
      node.appendChild(link);
    });

  })();
</script>
    </body>
    <!-- Theme: Elegant built for Pelican
    License : http://oncrashreboot.com/pelican-elegant -->
</html>